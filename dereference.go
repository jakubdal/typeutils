package typeutils

import "reflect"

// Dereference dereferences entity (extracts underlying element, unless the pointer is nil)
func Dereference(entity interface{}) interface{} {
	entityCopy := entity
	for entityCopy != nil &&
		reflect.TypeOf(entityCopy).Kind() == reflect.Ptr &&
		!reflect.ValueOf(entityCopy).IsNil() {
		entityCopy = reflect.ValueOf(entityCopy).Elem().Interface()
	}
	return entityCopy
}
