package typeutils

import (
	"reflect"
	"testing"
)

func TestDereference(t *testing.T) {
	str := struct{}{}
	str1 := &str
	str2 := &str1
	var iface interface{}
	ifacePtr := &iface
	text := "text"
	textPtr := &text
	tests := []struct {
		name string

		in  interface{}
		out interface{}
	}{
		{
			name: "string",

			in:  text,
			out: text,
		}, {
			name: "*string",

			in:  textPtr,
			out: text,
		}, {
			name: "struct",

			in:  str,
			out: str,
		}, {
			name: "*struct",

			in:  str1,
			out: str,
		}, {
			name: "**struct",

			in:  str2,
			out: str,
		}, {
			name: "*bool(nil)",

			in:  (*bool)(nil),
			out: (*bool)(nil),
		}, {
			name: "interface(nil)",

			in:  iface,
			out: iface,
		}, {
			name: "*interface(nil)",

			in:  ifacePtr,
			out: iface,
		},
	}

	for _, tc := range tests {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()

			if out := Dereference(tc.in); !reflect.DeepEqual(out, tc.out) {
				t.Errorf("invalid out, expected %v got %v", tc.out, out)
			}
		})
	}
}
