package typeutils

import (
	"reflect"
	"strings"
	"testing"
)

func TestStringSliceUnique(t *testing.T) {
	tests := []struct {
		in  []string
		out []string
	}{
		{},
		{
			in:  []string{},
			out: []string{},
		},
		{
			in:  []string{"a", "b", "c"},
			out: []string{"a", "b", "c"},
		},
		{
			in:  []string{"a", "b", "b", "c", "a"},
			out: []string{"a", "b", "c"},
		},
	}

	for _, tc := range tests {
		tc := tc
		t.Run(strings.Join(tc.in, ", "), func(t *testing.T) {
			t.Parallel()

			if out := StringSliceUnique(tc.in); !reflect.DeepEqual(out, tc.out) {
				t.Errorf("invalid out\nexpected:\n\t%+#v\ngot:\n\t%+#v", tc.out, out)
			}
		})
	}
}

func TestStringSliceIntersection(t *testing.T) {
	tests := []struct {
		name string

		original []string
		limiter  []string
		out      []string
	}{
		{
			name:     "one element slices",
			original: []string{"a", "a", "a"},
			limiter:  []string{"a", "a", "a"},
			out:      []string{"a", "a", "a"},
		},
		{
			name:     "intersection = 0",
			original: []string{"a", "b", "c"},
			limiter:  []string{"d", "e", "f"},
			out:      []string{},
		},
		{
			name:     "intersection = all",
			original: []string{"a", "b", "c"},
			limiter:  []string{"a", "b", "c"},
			out:      []string{"a", "b", "c"},
		},
		{
			name:     "limiter is subset of original",
			original: []string{"a", "b", "c"},
			limiter:  []string{"a", "b"},
			out:      []string{"a", "b"},
		},
		{
			name:     "limiter is superset of original",
			original: []string{"a", "b", "c"},
			limiter:  []string{"a", "b", "c", "d", "e"},
			out:      []string{"a", "b", "c"},
		},
		{
			name:    "original - nil",
			limiter: []string{"a", "b", "c"},
			out:     []string{},
		},
		{
			name:     "limiter - nil",
			original: []string{"a", "b", "c"},
			out:      []string{},
		},
		{
			name: "both slices - nil",
			out:  []string{},
		},
	}

	for _, tc := range tests {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()
			out := StringSliceIntersection(tc.original, tc.limiter)
			if !reflect.DeepEqual(out, tc.out) {
				t.Errorf("invalid out\nexpected:\n\t%+#v\ngot:\n\t%+#v", tc.out, out)
			}
		})
	}
}

func TestStringSliceSum(t *testing.T) {
	tests := []struct {
		name string

		slice1 []string
		slice2 []string
		out    []string
	}{
		{
			name:   "one element slices",
			slice1: []string{"a", "a", "a"},
			slice2: []string{"a", "a", "a"},
			out:    []string{"a"},
		},
		{
			name:   "intersection = 0",
			slice1: []string{"a", "b", "c"},
			slice2: []string{"d", "e", "f"},
			out:    []string{"a", "b", "c", "d", "e", "f"},
		},
		{
			name:   "intersection = all",
			slice1: []string{"a", "b", "c"},
			slice2: []string{"a", "b", "c"},
			out:    []string{"a", "b", "c"},
		},
		{
			name:   "slice2 is subset of slice1",
			slice1: []string{"a", "b", "c"},
			slice2: []string{"a", "b"},
			out:    []string{"a", "b", "c"},
		},
		{
			name:   "slice2 is superset of slice1",
			slice1: []string{"a", "b", "c"},
			slice2: []string{"a", "b", "c", "d", "e"},
			out:    []string{"a", "b", "c", "d", "e"},
		},
		{
			name:   "slice1 - nil",
			slice2: []string{"a", "b", "c"},
			out:    []string{"a", "b", "c"},
		},
		{
			name:   "slice2 - nil",
			slice1: []string{"a", "b", "c"},
			out:    []string{"a", "b", "c"},
		},
		{
			name: "both slices - nil",
			out:  []string{},
		},
	}

	for _, tc := range tests {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()
			out := StringSliceSum(tc.slice1, tc.slice2)
			if !reflect.DeepEqual(out, tc.out) {
				t.Errorf("invalid out\nexpected:\n\t%+#v\ngot:\n\t%+#v", tc.out, out)
			}
		})
	}
}
