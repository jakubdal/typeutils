# Type Utils

`typeutils` is a package with utility functions that help with Golang types

## Installation

Run `go get -u gitlab.com/jakubdal/typeutils` to import `typeutils` to your project.
