package typeutils

// StringSliceUnique returns slice of unique elements from given slice
func StringSliceUnique(slice []string) []string {
	if slice == nil {
		return nil
	}

	out := make([]string, 0, len(slice))
	usedElems := map[string]struct{}{}

	for _, elem := range slice {
		if _, exists := usedElems[elem]; exists {
			continue
		}
		usedElems[elem] = struct{}{}
		out = append(out, elem)
	}

	return out
}

// StringSliceSum returns slice of unique elements from all of the slices
func StringSliceSum(slices ...[]string) []string {
	sliceSum := []string{}
	usedElems := map[string]struct{}{}

	for _, slice := range slices {
		for _, elem := range slice {
			if _, ok := usedElems[elem]; ok {
				continue
			}
			sliceSum = append(sliceSum, elem)
			usedElems[elem] = struct{}{}
		}
	}

	return sliceSum
}

// StringSliceIntersection returns elements from `original` slice, that also exist in
// `limiter` slice, preserving order
//
// It could also be described as - returns `original` slice without elements that do not
// exist in `limiter` slice.
func StringSliceIntersection(original, limiter []string) []string {
	sliceIntersection := make([]string, 0, len(original))

	limiterElements := make(map[string]struct{})
	for _, elem := range limiter {
		limiterElements[elem] = struct{}{}
	}
	for _, elem := range original {
		if _, ok := limiterElements[elem]; ok {
			sliceIntersection = append(sliceIntersection, elem)
		}
	}

	return sliceIntersection
}
